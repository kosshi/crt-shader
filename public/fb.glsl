#ifdef VERTEX_SHADER

attribute vec2 a_vert;
attribute vec2 a_uv;
varying vec2 v_uv;

void main(void) {
	gl_Position = vec4(a_vert, 0.5, 1.0);
	v_uv = a_uv;
}

#else

precision highp float;

uniform sampler2D u_texture;
varying vec2 v_uv;

void main(void) {

	gl_FragColor = vec4(texture2D(u_texture, vec2(v_uv.x, 1.0-v_uv.y)).rgb, 1.0 );

}

#endif
