#ifdef VERTEX_SHADER

attribute vec2 a_vert;

attribute vec2 a_uv;

varying vec2 v_uv;

void main(void) {
	gl_Position = vec4(a_vert, 0.5, 1.0);
	v_uv = a_uv;
}

#else

precision highp float;

#define M_PI 3.1415926535897932384626433832795

uniform sampler2D u_texture;

uniform float u_lines;
uniform float u_corner;
uniform float u_zoom;
uniform float u_shape;
uniform float u_round;

uniform float u_grain;
uniform float u_vpitch;
uniform float u_hpitch;


uniform float u_imgx;
uniform float u_imgy;
uniform float u_imgw;
uniform float u_imgh;

uniform float u_img_gain;
uniform float u_img_bias;


uniform float u_beam_gain;
uniform float u_beam_bias;


uniform float u_top;
uniform float u_bot;

varying vec2 v_uv;


float max3 (vec3 v) {
  return max (max (v.x, v.y), v.z);
}


void main(void) {

	vec2 uv_orig = (v_uv * 2.0 - 1.0);

	vec2 uv_mod = uv_orig * pow(1.0-abs(uv_orig),vec2(u_round)) * (u_zoom + u_corner * pow( abs(uv_orig.yx), vec2(u_shape)) );


	vec2 uv = uv_mod / 2.0 + 0.5;

	if ( abs(uv_mod).x > 1.0 || abs(uv_mod.y) > 1.0 ) {
		gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
		return;
	}

	float spacing = 1.0/u_lines;
	uv+=spacing*0.5;

	float line_top = ( ceil(uv.y*u_lines)) / u_lines;
	float line_bot = line_top - spacing;

	vec2 scale = vec2(u_imgw, u_imgh);
	vec2 offset = vec2(u_imgx, u_imgy);

	vec2 uv_top = (vec2(uv.x, line_top)+offset) * scale - (scale-1.0)*0.5 ;
	vec2 uv_bot = (vec2(uv.x, line_bot)+offset) * scale - (scale-1.0)*0.5 ;

	uv_top -= spacing * 0.5;
	uv_bot -= spacing * 0.5;

	vec3 color_top = texture2D(u_texture, uv_top).xyz * u_img_gain + u_img_bias;
	vec3 color_bot = texture2D(u_texture, uv_bot).xyz * u_img_gain + u_img_bias;

	float dist_top = pow(abs(uv.y - line_top), 1.0);
	float dist_bot = pow(abs(uv.y - line_bot), 1.0);

	vec3 beam_top = 1.0 - (dist_top / (spacing * (color_top * u_beam_gain + u_beam_bias)));
	vec3 beam_bot = 1.0 - (dist_bot / (spacing * (color_bot * u_beam_gain + u_beam_bias)));
	
	beam_top = clamp(beam_top, 0.0, 1.0) ;
	beam_bot = clamp(beam_bot, 0.0, 1.0) ;

	vec3 color = (color_top*beam_top)*u_top + (color_bot*beam_bot)*u_bot;

	vec2 dot;
	dot.y = floor(uv.y * u_vpitch) ;
	dot.x = uv.x;

	if (mod(dot.y, 2.0) > 0.5)
		dot.x += (4.5/3.0) / u_hpitch;

	dot.x = (floor(dot.x*u_hpitch)) ;

	int filter = int(mod(dot.x, 3.0));
	
	vec3 out_color = color * (1.0-u_grain);
	
	vec3 passthru = vec3( 
		float(filter == 0), 
		float(filter == 1),
		float(filter == 2)
	) * (u_grain);

	out_color += color * passthru;

	gl_FragColor = vec4((out_color), 1.0 );
}

#endif
