'use strict';

let canvas = document.querySelector("canvas#primary");

let adjustments = [
	"lines",
	
	"imgx",
	"imgy",
	"imgw",
	"imgh",
	"img_gain",
	"img_bias",

	"beam_bias",
	"beam_gain",

	"corner",
	"zoom",
	"shape",
	"round",

	"grain",
	"vpitch",
	"hpitch",

	"top",
	"bot"
];

let dom = {
	fps:    document.querySelector("#fps"),
	image:  document.querySelector("#image"),
	loadgrid:  document.querySelector("#loadgrid"),
	loadbars:  document.querySelector("#loadbars"),
	loadsmpte: document.querySelector("#loadsmpte"),

	res:     document.querySelector("#res"),
	res_set: document.querySelector("#res_set"),
	supersample: document.querySelector("#supersample"),
	download: document.querySelector("#download"),
}
adjustments.forEach(v=>{
	dom[v] = document.querySelector("#"+v);
});



let quad_vertex = new Float32Array([
	-1, -1,
	-1,  1,
	 1, -1,
	 1,  1
]);

let quad_uv = new Float32Array([
	0, 1,
	0, 0,
	1, 1,
	1, 0
]);

let gl;
let quad = {};
let shaders = {};
let textures = {};
let framebuffers = {};
let supersample = parseFloat(dom.supersample.value);


function main()
{
	canvas.width  = 1280;
	canvas.height = 960;

	try {
		gl = canvas.getContext("webgl2");
	} catch (e) {
	}
	if (!gl) {
		alert("Could not initialise WebGL 2.0");
		throw "Could not initialise WebGL 2.0";
	} else {
		console.log("GL OK");
	}
	
	textures.supersample = gl.createTexture();
	framebuffers.supersample = gl.createFramebuffer();

	update_framebuffers();


	quad = {
		vertex: gl.createBuffer(),
		uv: gl.createBuffer()
	};

	gl.clearColor(0.0, 0.1, 0.0, 1.0);
	gl.disable(gl.DEPTH_TEST);
	gl.disable(gl.CULL_FACE);

	/*gl.enable(gl.SAMPLE_COVERAGE);
	gl.sampleCoverage(0.25, true);*/

	quad.vertex = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, quad.vertex);
	gl.bufferData(gl.ARRAY_BUFFER, quad_vertex, gl.STATIC_DRAW);

	quad.uv = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, quad.uv);
	gl.bufferData(gl.ARRAY_BUFFER, quad_uv, gl.STATIC_DRAW);

	textures.test = gl.createTexture();
	load_texture(gl, textures.test, "./img/grid.png");

	let uniforms = [
		['texture', 'u_texture'],
	];

	adjustments.forEach(a=>{
		uniforms.push([a, 'u_'+a]);
	});

	shaders.test = new Shader(gl, 
		'./crt.glsl',
		[
			['a_vert','a_vert'],
			['a_uv',  'a_uv']
		],
		uniforms
	);
	
	shaders.supersample = new Shader(gl,
		'./fb.glsl',
		[
			['a_vert', 'a_vert'], 
			['a_uv',  'a_uv']
		],
		[
			['texture', 'u_texture'],
		]
	);

	tick();
}


function update_framebuffers(){
	gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffers.supersample);

	let w = canvas.width  * supersample;
	let h = canvas.height * supersample;

	gl.bindTexture(gl.TEXTURE_2D, textures.supersample);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, w, h, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, textures.supersample, 0);
}

dom.loadgrid.onclick  = ()=>{load_texture(gl, textures.test, "img/grid.png");}
dom.loadbars.onclick  = ()=>{load_texture(gl, textures.test, "img/bars.png");}
dom.loadsmpte.onclick = ()=>{load_texture(gl, textures.test, "img/smpte.png");}

dom.download.onclick = e=>
{
	tick();
	let link = document.createElement('a');
	link.download = 'kosshi_gitlab_io_crt-shader.png';
	link.href = canvas.toDataURL();
	link.click();
}

dom.res_set.onclick = e=>
{	
	let h = parseInt(dom.res.value);
	let w = h * (4/3);
	canvas.height = h;
	canvas.width = w;

	update_framebuffers();
}

dom.supersample.onchange = e=>{
	console.log("Update supersampling");
	supersample = parseFloat(dom.supersample.value);
	update_framebuffers();
}

dom.image.onchange = e => {
	const [file] = dom.image.files;
	if (!file) return;

	let reader = new FileReader();
	
	reader.onload = (e)=>{

		let image = new Image()
		
		image.onload = ()=>{
			
			gl.bindTexture(gl.TEXTURE_2D, textures.test);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

			gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		}
		image.src = reader.result;

	}

	reader.readAsDataURL(file);
}

function load_texture(gl, texture, url) 
{
	gl.bindTexture(gl.TEXTURE_2D, texture);

	const level = 0;
	const internalFormat = gl.RGBA;
	const width = 1;
	const height = 1;
	const border = 0;
	const srcFormat = gl.RGBA;
	const srcType = gl.UNSIGNED_BYTE;
	const pixel = new Uint8Array([0, 0, 255, 255]);  
	gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, pixel);

	const image = new Image();
	image.onload = () => {
		
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image);

		gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D,  gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	};

	image.src = url;

}


let stats = {
	last: 0,
	frames: 0,
};

function tick()
{
	draw();
	stats.frames++;

	let now = new Date().getTime();
	if (now > stats.last+1000) {
		dom.fps.innerHTML = stats.frames;
		
		stats.frames = 0;
		stats.last = now;
	}
	requestAnimationFrame(tick);
}

function draw()
{
	let shader = shaders.test;
	if(!shader.ready) return;
	gl.useProgram(shader.program);
	shader.enableAttributes();

	gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffers.supersample);
	gl.drawBuffers([gl.COLOR_ATTACHMENT0]);

	gl.viewport(0, 0, canvas.width*supersample, canvas.height*supersample);

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, textures.test);
	gl.uniform1i(shader.uniform.texture, 0);

	gl.bindBuffer(gl.ARRAY_BUFFER, quad.vertex);
	gl.vertexAttribPointer(shader.attribute.a_vert, 2, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, quad.uv);
	gl.vertexAttribPointer(shader.attribute.a_uv, 2, gl.FLOAT, false, 0, 0);


	adjustments.forEach(a=>{
		gl.uniform1f(shader.uniform[a], parseFloat(dom[a].value));
	});

	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

	shader.disableAttributes();

	
	/* Output */


	shader = shaders.supersample;
	if(!shader.ready) return;
	gl.useProgram(shader.program);
	shader.enableAttributes();

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.viewport(0, 0, canvas.width, canvas.height);

	gl.bindBuffer(gl.ARRAY_BUFFER, quad.vertex);
	gl.vertexAttribPointer(shader.attribute.a_vert, 2, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, quad.uv);
	gl.vertexAttribPointer(shader.attribute.a_uv, 2, gl.FLOAT, false, 0, 0);


	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, textures.supersample);
	gl.uniform1i(shader.uniform.texture, 0);


	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

	shader.disableAttributes();
}


main();
